#!/usr/bin/perl -w
use v5.18;
use Tk;

my $mw = MainWindow->new;
$mw->Button(-text => "\U$_", -command => \&exit)->pack(-side => $_, -fill => 'both')
    for qw(top bottom right left);

my $tl = $mw->Toplevel;
$tl->Button(-text => "\U$_", -command => \&exit)->pack(-side => $_, -fill => 'both')
    for qw(right left top bottom);

$tl = $mw->Toplevel;
$tl->Button(-text => "\U$_", -command => \&exit)->pack(-side => $_, -fill => 'both')
    for qw(top right bottom left);

MainLoop;

