#!/usr/bin/perl -w
use v5.18;
use Tk;

my $mw = MainWindow->new;
$mw->Button(-text => "\U$_", -command => \&exit)
    ->pack(-side => $_)
        for qw(top bottom right left);

MainLoop;
