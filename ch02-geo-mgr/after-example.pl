#!/usr/bin/perl -w
use v5.18;
use Tk;

my $mw = MainWindow->new;
my $one = $mw->Button(-text => 'one')->pack(-side => 'left');
my $two = $mw->Button(-text => 'two')->pack(-side => 'left');
my $three = $mw->Button(-text => 'three')->pack(-side => 'left');
my $four = $mw->Button(-text => 'four')->pack(-after => $one, -side => 'left');

MainLoop;

