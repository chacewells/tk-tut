#!/usr/bin/perl -w
use v5.18;
use Tk;

my $mw = MainWindow->new;
my $label = $mw->Label(-text => 'Here I am!');
$mw->Button(-text => 'Show!', -command => sub { $label->pack if defined $label; undef $label })->pack;

MainLoop;
