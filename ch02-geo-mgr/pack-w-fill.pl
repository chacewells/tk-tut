#!/usr/bin/perl -w
use v5.18;
use Tk;

my $mw = MainWindow->new;
$mw->title('What, me worry?');
$mw->Label(-text => 'Oh yeah. What you want now?')->pack;
$mw->Button(-text => 'Exit', -command => \&exit)->pack(-fill => 'both');
MainLoop;
