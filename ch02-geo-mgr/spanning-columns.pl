#!/usr/bin/perl -w
use v5.18;
use Tk;

my $mw = MainWindow->new;
$mw->Button(-text => "Button1", -command => sub { exit })->grid
  ($mw->Button(-text => "Button2", -command => sub { exit }),
   $mw->Button(-text => "Button3", -command => sub { exit }),
   $mw->Button(-text => "Button4", -command => sub { exit }));

$mw->Button(-text => "Button5", -command => sub { exit })->grid
  ($mw->Button(-text => "Button6", -command => sub { exit }),
   "^",
   $mw->Button(-text => "Button8", -command => sub { exit }));


$mw->Button(-text => "Button9", -command => sub { exit })->grid
  ($mw->Button(-text => "Button10", -command => sub { exit }),
   ("-") x 2,  -sticky => "nsew");

MainLoop;
