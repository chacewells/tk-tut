#!/usr/bin/perl -w
use v5.18;
use Tk;

my @game_state;
my @marks = qw(X 0);
my $player = 0;
my $game_over = 0;

my $mw = MainWindow->new;
$mw->title('Tic Tac Toe');

for my $row (1..3) {
for my $col (1..3) {
    my $btn;
    $btn = $mw->Button(-command => sub {
        update_game($btn, $row, $col);
    })->grid(-row => $row, -column => $col);
}
}
my $notification = $mw->Label(-text => "Your turn, $marks[$player]")->grid( ('-') x 3 );

MainLoop;

sub update_game {
    return if $game_over;
    my ($btn, $row, $col) = @_;
    --$_ for $row, $col;
    if (cell_occupied($row, $col)) {
        $notification->configure(-text => 'Cell occupied! Try again!');
    } else {
        $game_state[$row][$col] = $marks[$player];
        $btn->configure(-text => $marks[$player]);
        if (check_winner($row, $col)) {
            $notification->configure(-text => "$marks[$player] wins!");
            $game_over = 1;
        } else {
            change_player();
            $notification->configure(-text => "Your turn, $marks[$player]");
        }
    }
}

sub cell_occupied { defined $game_state[shift][shift] }

sub change_player {
    $player = ($player + 1) % 2;
}

sub check_winner {
    my ($row, $col) = @_;
    check_row($row)
    || check_col($col)
    || check_diag($row, $col)
    || check_antidiag($row, $col)
}

sub check_row {
    my $row = shift;
    my $last = $game_state[$row][0] // return 0;
    for my $col(1, 2) {
        return 0 unless defined $game_state[$row][$col] and $last eq $game_state[$row][$col];
        $last = $game_state[$row][$col];
    }

    return 1;
}

sub check_col {
    my $col = shift;
    my $last = $game_state[0][$col] // return 0;
    for my $row(1, 2) {
        return 0 unless defined $game_state[$row][$col] and $last eq $game_state[$row][$col];
        $last = $game_state[$row][$col];
    }

    return 1;
}

sub check_diag {
    my ($row, $col) = @_;
    return 0 unless $row == $col and defined $game_state[$row][$col];
    for (0..2) {
        next if $_ == $row;
        return 0 unless defined $game_state[$_][$_] and $game_state[$row][$col] eq $game_state[$_][$_];
    }

    return 1;
}

sub check_antidiag {
    my ($row, $col) = @_;
    return 0 unless $row == 2 - $col and defined $game_state[$row][$col];
    for (0..2) {
        next if $_ == $row;
        return 0 unless defined $game_state[$_][2-$_] and $game_state[$row][$col] eq $game_state[$_][2-$_];
    }

    return 1;
}

