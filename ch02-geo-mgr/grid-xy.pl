#!/usr/bin/perl -w
use v5.18;
use Tk;

my $mw = MainWindow->new;

for my $row (1..5) {
for my $col (1..5) {
    $mw->Button(-text => "$row, $col", -command => sub { say "row $row, column $col clicked" })->grid(-row => $row, -column => $col);
}
}

MainLoop;
